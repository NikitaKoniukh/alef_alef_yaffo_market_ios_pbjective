//
//  main.m
//  alef_alef_yafo_market1
//
//  Created by doron on 10/03/2019.
//  Copyright © 2019 nisancomp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
