//
//  AppDelegate.h
//  alef_alef_yafo_market1
//
//  Created by doron on 10/03/2019.
//  Copyright © 2019 nisancomp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

